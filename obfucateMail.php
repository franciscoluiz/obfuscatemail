<?php
	function obfuscateMail( $email ) {

     	$emailLetters = preg_split( '//u', $email, null, 1 );
		$obfuscatedEmail = '';
		$emailLetters = array_reverse( $emailLetters );
		$characters = '1234567890qwertzuiopasdfghjklyxcvbnmMNBVCXYLKJHGFDSAPOIUZTREWQ._-';
		$characters = str_shuffle(str_shuffle($characters));
		$charactersLength = strlen( $characters ) - 1;
		foreach( $emailLetters as $letter ) {
			$letterPos = strpos($characters, $letter);
			if( $letterPos !== false ) {
				$letterPos += $charactersLength / 2;
				$letterPos = $letterPos > $charactersLength ? $letterPos - $charactersLength - 1 : $letterPos; 
				$newLetter = substr($characters, $letterPos, 1);
			} else {
				$newLetter = $letter;
			}
			$obfuscatedEmail .= $newLetter;
		}
		$obfuscated = $obfuscatedEmail . '@';
		$randId = rand(1, 500);
		$span = '<spam id="obsmail-'.$randId.'">[habilite o javascript para visualizar o e-mail]</spam>';
        
		/*$jv = '<script>
		(function(){
			var span = document.getElementById("obsmail-'.$randId.'");
			//var text = span.innerHTML,
			var text = "'.$obfuscated.'",
				textReplace = text.search(/@.+@/);
			if( textReplace > -1 ) {
				string = text;
				var currentLetter, 
					currentPos,
					currentString = "",
					i = 0,
					stringLength = string.length - 1,
					characters = "'.$characters.'",
					charactersLength = characters.length;
				for( ; i<stringLength; i++ ) {
					currentLetter = string.charAt(i);
					currentPos = characters.indexOf(currentLetter);
					if( currentPos > -1 ) {
						currentPos -= (charactersLength-1) / 2;
						currentPos = currentPos < 0 ? charactersLength + currentPos : currentPos;
					} else {
						currentString += currentLetter;
					}
					currentString += characters.charAt(currentPos);
				}
				text = currentString;

				var a = text;
				var b = String.fromCharCode(60,115,112,97,110,32,115,116,121,108,101,61,34,100,105,115,112,108,97,121,58,32,110,111,110,101,59,34,62,110,117,108,108,60,47,115,112,97,110,62);
				
				//var randPos = Math.floor(Math.random() * (a.length));
				//if (randPos == 0) { randPos = 1;}
				//var c = a.substr(0, randPos) + b + a.substr(randPos);
				
				var d = a.split("@");
		
				c = d[0]+b+String.fromCharCode(64)+b+d[1];
								
				span.innerHTML = c;
				span.style.direction = "rtl";
				span.style.unicodeBidi = "bidi-override";
			}
		}()); 
		</script>'; */
		
		// optimizado aqui: http://closure-compiler.appspot.com/home
		$jv = '<script>(function(){var e=document.getElementById("obsmail-'.$randId.'"),b="'.$obfuscated.'";if(-1<b.search(/@.+@/)){string=b;for(var a,d="",f=0,g=string.length-1;f<g;f++)b=string.charAt(f),a="'.$characters.'".indexOf(b),-1<a?(a-=32,a=0>a?65+a:a):d+=b,d+="'.$characters.'".charAt(a);a=d;b=String.fromCharCode(60,115,112,97,110,32,115,116,121,108,101,61,34,100,105,115,112,108,97,121,58,32,110,111,110,101,59,34,62,110,117, 108,108,60,47,115,112,97,110,62);a=a.split("@");c=a[0]+b+String.fromCharCode(64)+b+a[1];e.innerHTML=c;e.style.direction="rtl";e.style.unicodeBidi="bidi-override"}})();</script>';
			
		return $span.$jv;
		
	}
?>